# [![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/breatfr) <a href="https://www.paypal.me/breat"><img src="https://github.com/andreostrovsky/donate-with-paypal/raw/master/blue.svg" alt="PayPal" height="30"></a>
Le site [SuperSoluce](https://supersoluce.com/) est mieux adapté aux écrans larges.
## Prévisualisation
![Preview](https://gitlab.com/breatfr/supersoluce/-/raw/main/docs/preview.jpg)

## Comment l'utiliser en quelques étapes
1. Installer l'extension de navigateur Stylus
    - Lien pour les navigateurs basés sur Chromium : https://chrome.google.com/webstore/detail/stylus/clngdbkpkpeebahjckkjfobafhncgmne
        - Brave
        - Chromium
        - Google Chrome
        - Iridium Browser
        - Microsoft Edge
        - Opera
        - Opera GX
        - SRWare Iron
        - Ungoogled Chromium
        - Vivaldi
        - Yandex Browser
        - bien d'autres
    - Lien pour les navigateurs basés sur Firefox : https://addons.mozilla.org/firefox/addon/styl-us/
        - Mozilla Firefox
        - Mullvad Browser
        - Tor Browser
        - Waterfox
        - bien d'autres

2. Allez sur le site [UserStyles.world](https://userstyles.world/style/16626) et cliquez sur `Install` sous l'image ou ouvrir la [version GitLab](https://gitlab.com/breatfr/supersoluce/-/raw/main/css/supersoluce-responsive.user.css).

3. Pour mettre à jour le thème, ouvrez la fenêtre `Gestion des styles` et cliquez sur `Vérifier la mise à jour` et suivez les instructions ou attendez simplement 24h pour la mise à jour automatique.

4. Profitez :)
# [![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/breatfr) <a href="https://www.paypal.me/breat"><img src="https://github.com/andreostrovsky/donate-with-paypal/raw/master/blue.svg" alt="PayPal" height="30"></a>